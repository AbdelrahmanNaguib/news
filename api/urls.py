from django.urls import path
from .views import  ArticleListView,ArticleDetailView,UserViewset,UserDetailViewset

urlpatterns = [
    path("", ArticleListView.as_view()),
    path("<int:pk>/", ArticleDetailView.as_view()),
    path("users/", UserViewset.as_view()),
    path("users/<int:pk>/", UserDetailViewset.as_view())
]